export default class {
  async *parse(stream) {
    const textDecoder = new TextDecoder()

    let timestamped = null

    for await (const chunk of stream) {
      const chunkString = textDecoder.decode(chunk)
      const lines = chunkString.split('\n')

      for (const line of lines) {
        if (line.trim() === '') continue


        // determine if this is a timestamped log
        if (timestamped === null) {
          timestamped = !!Date.parse(line.slice(0, 27))
        }

        if (timestamped) {
          yield {
            date: line.slice(0, 27),
            streamID: line.slice(28, 30),
            streamType: line[30],
            append: line[31] == '+',
            text: line.slice(32),
          }
        } else {
          yield {
            text: line
          }
        }
      }
    }
  }
}
