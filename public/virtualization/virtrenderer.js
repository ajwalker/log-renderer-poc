const lineHeight = 20
const linesPerSection = 100

class LogSection {
  constructor(parent, idx) {
    this.element = document.createElement('div')
    this.element.style.height = `${linesPerSection * lineHeight}px`
    this.element.dataset.section = idx

    this.wrapper = document.createElement('div')

    parent.appendChild(this.element)
  }

  lines() {
    return this.wrapper.children.length
  }

  add(element) {
    this.wrapper.appendChild(element)
  }

  show() {
    this.element.style.height = 'auto'
    if (this.element.children.length === 0) {
      this.element.appendChild(this.wrapper)
    }
  }

  hide() {
    this.element.style.height = `${this.element.offsetHeight}px`
    if (this.element.children.length > 0 && !this.hasSelectedText()) {
      this.element.removeChild(this.wrapper)
    }
  }

  height() {
    return this.element.offsetHeight
  }

  hasSelectedText() {
    const selection = window.getSelection()
    if (!selection || selection.rangeCount === 0) {
      return false
    }

    const range = selection.getRangeAt(0)
    const { startContainer, endContainer } = range

    // text within element selected
    if (this.element.contains(startContainer) || this.element.contains(endContainer)) {
      return true
    }

    // whole element selected
    return this.element.compareDocumentPosition(startContainer) & Node.DOCUMENT_POSITION_PRECEDING &&
      this.element.compareDocumentPosition(endContainer) & Node.DOCUMENT_POSITION_FOLLOWING
  }
}

export default class {
  constructor(container) {
    this.container = container
    this.sections = []
    this.lines = 1
    this.refs = []

    document.addEventListener('scroll', this.update.bind(this))
    document.addEventListener('copy', this.copy.bind(this))
  }

  addContent(line) {
    const element = document.createElement('div')
    element.className = 'log-line'

    if (line.sections && !line.header) {
      // using className, with the absolute guess that it's faster
      // to search for elements with getElementsByClassName than
      // it is using data-name
      element.className += ' ' + line.sections.join(' ')
    }

    if (line.header) {
      const button = document.createElement('button')
      button.textContent = '⌄'

      button.addEventListener('click', () => {
        this.toggle(button, line.header)
      })
      element.appendChild(button)
    }

    const a = document.createElement('a')
    a.className = "number"
    a.textContent = this.lines++
    a.id = 'L' + a.textContent
    a.href = '#' + a.id
    element.appendChild(a)

    for (let idx in line.content) {
      let item = line.content[idx]

      const span = document.createElement('span')
      if (item.style) {
        span.className = item.style
      }
      span.textContent = item.text
      element.appendChild(span)
    }

    if (this.sections.length == 0 || this.sections[this.sections.length - 1].lines() === linesPerSection) {
      this.sections.push(new LogSection(this.container, this.sections.length, linesPerSection, lineHeight))
    }

    this.refs.push(element)
    this.sections[this.sections.length - 1].add(element)
  }

  toggle(button, header) {
    if (button.textContent === '⌄') {
      button.textContent = '›'

      this.refs.forEach(e => {
        if (e.classList.contains(header)) {
          e.style.display = 'none'
        }
      })

    } else {
      button.textContent = '⌄'

      this.refs.forEach(e => {
        if (e.classList.contains(header)) {
          e.style.display = ''
        }
      })
    }

    this.update()
  }

  copy() {
    // display any elements that have text selected,
    // this can be a pretty hefty operation is many sections are selected,
    // but copying a lot of data is an expectedly hefty operation sometimes.
    for (let idx in this.sections) {
      if (this.sections[idx].hasSelectedText()) {
        this.sections[idx].show()
      }
    }
  }

  update() {
    let fullHeight = this.container.offsetTop
    for (let idx = 0; idx < this.sections.length; idx++) {
      let section = this.sections[idx]
      let height = section.height()

      if (document.documentElement.scrollTop > fullHeight - document.documentElement.clientHeight && document.documentElement.scrollTop < fullHeight + height) {
        section.show()
      } else {
        section.hide()
      }

      fullHeight += section.height()
    }
  }
}
