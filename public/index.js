import LineReader from './linereader.js'
import Scanner from './scanner.js'
import BasicRenderer from './basicrenderer.js'

const logStream = fetch("./job.txt")
  .then((response) => {
    const reader = response.body.getReader()

    async function* streamGenerator() {
      while (true) {
        const { done, value } = await reader.read()
        if (done) break
        yield value
      }
    }

    return streamGenerator()
  });

(async () => {
  const container = document.getElementById('logContainer')
  const renderer = new BasicRenderer(container)

  const lineReader = new LineReader()
  const scanner = new Scanner()

  for await (const line of lineReader.parse(await logStream)) {
    renderer.addContent(scanner.scan(line.text))
  }
})()
