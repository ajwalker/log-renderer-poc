export default class {
  constructor(container) {
    this.container = container
    this.lines = 1
    this.refs = []
  }

  addContent(line) {
    const element = document.createElement('div')
    element.className = 'log-line'

    if (line.sections && !line.header) {
      // using className, with the absolute guess that it's faster
      // to search for elements with getElementsByClassName than
      // it is using data-name
      element.className += ' ' + line.sections.join(' ')
    }

    if (line.header) {
      const button = document.createElement('button')
      button.textContent = '⌄'

      button.addEventListener('click', () => {
        this.toggle(button, line.header)
      })
      element.appendChild(button)
    }

    const a = document.createElement('a')
    a.className = "number"
    a.textContent = this.lines++
    a.id = 'L' + a.textContent
    a.href = '#' + a.id
    element.appendChild(a)

    for (let idx in line.content) {
      let item = line.content[idx]

      const span = document.createElement('span')
      if (item.style) {
        span.className = item.style
      }
      span.textContent = item.text
      element.appendChild(span)
    }

    this.refs.push(element)
    this.container.appendChild(element)
  }

  toggle(button, header) {
    if (button.textContent === '⌄') {
      button.textContent = '›'

      this.refs.forEach(e => {
        if (e.classList.contains(header)) {
          e.style.display = 'none'
        }
      })
    } else {
      button.textContent = '⌄'

      this.refs.forEach(e => {
        if (e.classList.contains(header)) {
          e.style.display = ''
        }
      })
    }
  }
}
