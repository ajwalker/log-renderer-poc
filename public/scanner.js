import Ansi from './ansi.js'

export default class {
  constructor() {
    this.ansi = new Ansi()
    this.reset()
  }

  reset() {
    this.ansi.reset()
    this.content = []
    this.sections = []
  }

  scan(input) {
    let start = 0
    let offset = 0

    while (offset < input.length) {
      // find ansi csi
      if (input.startsWith('\u001b[', offset)) {
        this._append(input.slice(start, offset))

        let op
        [op, offset] = this._parse_ansi(input, offset + 2)

        if (op.length > 0 && op.endsWith('m')) {
          this.ansi.evaluate(op.slice(0, -1).split(';'))
        }

        start = offset
        continue
      }

      // parse sections
      if (input.startsWith('section_', offset)) {
        this._append(input.slice(start, offset))

        let section
        [section, offset] = this._parse_section(input, offset + 8)

        if (section !== null) {
          this._section(section[0], section[1], section[2])
        }

        start = offset
        continue
      }

      offset++
    }

    this._append(input.slice(start, offset))

    let sectionHeader = ''
    if (this.sections.length > 0) {
      const section = this.sections[this.sections.length - 1]

      if (section.start) {
        sectionHeader = section.name
      }
      section.start = false
    }

    let content = this.content
    this.content = []

    return {
      sections: this.sections.map(obj => obj.name),
      header: sectionHeader,
      content: content
    }
  }

  _append(text) {
    text = text.trim()
    if (text.length === 0) {
      return
    }

    this.content.push({
      style: this.ansi.getClasses().join(' '),
      text: text,
    })
  }

  _section(type, time, name) {
    switch (type) {
      case 'start':
        this.sections.push({ name, time, start: true })
        break

      case 'end':
        if (this.sections.length === 0) {
          break
        }

        let section = this.sections[this.sections.length - 1]

        if (section.name == name) {
          let diff = time - section.time
          this.content.push({ section: name, duration: diff })
          this.sections.pop()
        }

        break
    }
  }

  _parse_ansi(input, offset) {
    let start = offset

    // find any number of parameter bytes (0x30-0x3f)
    for (; offset < input.length; offset++) {
      let c = input.charCodeAt(offset)
      if (c >= 0x30 && c <= 0x3f) {
        continue
      }
      break
    }

    // any number of intermediate bytes (0x20–0x2f)
    for (; offset < input.length; offset++) {
      let c = input.charCodeAt(offset)
      if (c >= 0x20 && c <= 0x2f) {
        continue
      }
      break
    }

    // single final byte (0x40–0x7e)
    let c = input.charCodeAt(offset)
    if (c >= 0x40 && c <= 0x7e) {
      offset++
    }

    return [input.slice(start, offset), offset]
  }

  _parse_section(input, offset) {
    let start = offset

    for (; offset < input.length; offset++) {
      let c = input[offset]

      // if not alphanumeric, '_', '-', ':' then break
      if ((c < '0' || c > '9') && (c < 'A' || c > 'Z') && (c < 'a' || c > 'z') && c !== '-' && c !== '_' && c !== ':') {
        break
      }
    }

    let section = input.slice(start, offset).split(':')
    if (section.length == 3) {
      return [section, offset]
    }

    return [null, offset]
  }
}

